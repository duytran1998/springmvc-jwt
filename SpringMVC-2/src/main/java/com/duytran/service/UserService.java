package com.duytran.service;

import com.duytran.entity.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    void insertUser(User user);
}
