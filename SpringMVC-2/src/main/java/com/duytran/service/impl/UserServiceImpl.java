package com.duytran.service.impl;


import com.duytran.service.UserService;
import com.duytran.entity.User;
import com.duytran.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;

    @Transactional
    public void insertUser(User user) {
        userRepository.insert(user);
    }
}
