package com.duytran.entity;

public class Employee {
    private int id;

    private String name;

    public Employee() {

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void getInfomationEmployee() {
        System.out.printf("ID: %d NAME: %s", this.id, this.name);
    }
}
