package com.duytran.controller;

import com.duytran.service.UserService;
import com.duytran.entity.User;
import com.duytran.model.LoginModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/auth")
public class AuthController {
    @Autowired
    UserService userService;

//    @Qualifier(value = "UserService")
//    public void setUserService(UserService userService){
//        this.userService = userService;
//    }


    @PostMapping("/login")
    public ResponseEntity<String> login(HttpServletRequest request, @RequestBody LoginModel loginModel) {
        System.out.println(loginModel);
        return ResponseEntity.ok("Hello");
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> register() {
        User user = new User("duytran", "123456");
        userService.insertUser(user);
        return ResponseEntity.ok("OK");
    }
}
