package com.duytran.controller;

import com.duytran.entity.Employee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/hello")
public class HelloController {
    @RequestMapping(method = RequestMethod.GET)
    public String printHelloWorld(ModelMap model) {
        model.addAttribute("message", "Hello String MVC");
        ApplicationContext context = new ClassPathXmlApplicationContext("IoC.xml");
        Employee employee = (Employee) context.getBean("employee");
        employee.setId(1);
        employee.setName("Duy");
        employee.getInfomationEmployee();
        return "index";
    }
}
